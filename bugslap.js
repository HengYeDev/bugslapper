var points = 0;
var time;

function update() {
	$("#points").html(points);
	$("#time").html(time);
}
function bug_onClick() {
	points += 100;
	update();
}
function getRandomPercentage() {
	return 1 + Math.floor(Math.random() * 99) + "%";
}
function startGame(amount, t) {
	$('#game').show();
	$("#start").hide();
	generateBugs(amount);
	time = t;
	update();
	countdown();
}
function generateBugs(amount) {
	for (var i = 2; i <= amount; i++) {
		$("#area").append('<img id="bug' + i + '" src="fly.png" class="bugs" />');
	}
	for (var j = 1; j <= amount; j++) {
		$("#bug" + j).css("top", getRandomPercentage());
		$("#bug" + j).css("left", getRandomPercentage())
		$("#bug" + j).click(function() {
			bug_onClick();
			$(this).hide();
		});
	}
	moveBugs(amount);
}
function moveBugs(amount) {
	setInterval(function() {
		for (var k = 1; k <= amount; k++) {
			var current = document.getElementById("bug" + k).style.top;
			var currentInt = current.substring(0, current.length - 1);

			var left = document.getElementById("bug" + k).style.left;
			var Left = left.substring(0, left.length - 1);

			if (Left > 100) {
				$("#bug" + k).css("left", Left % 100 + "%");
			}
			if (currentInt > 100) {
				$("#bug" + k).css("top", currentInt % 100 + "%");
			}
			var direction = Math.floor(Math.random() * 4);
			switch (direction) {
				

				case 1:
				if (!(currentInt > 90)) {
					console.log("hit after-break");
					var newInt2 = currentInt + 5;
					$("#bug" + k).css("top", newInt2 + "%");
					break;
				}
				
				case 0:
				
				if (currentInt < 10) {
					break;
				}
				var newInt = currentInt - 5;


				$("#bug" + k).css("top", newInt + "%");
				break;

				case 2:

				if (Left < 10) {
					break;
				}
				var newLeft = Left - 5;
				$("#bug" + k).css("left", newLeft + "%");
				break;

				case 3:

				var newLeft2 = Left + 5;
				$("#bug" + k).css("left", newLeft2 + "%");
				break;

			}
		}
	}, 500);
	setInterval(function() {
		for (var k = 1; k <= amount; k++) {
			var current = document.getElementById("bug" + k).style.top;
			var currentInt = current.substring(0, current.length - 1);

			var left = document.getElementById("bug" + k).style.left;
			var Left = left.substring(0, left.length - 1);

			if (Left > 100) {
				$("#bug" + k).css("left", Left % 100 + "%");
			}
			if (currentInt > 100) {
				$("#bug" + k).css("top", currentInt % 100 + "%");
			}
		}
	}, 1);
}
function finishGame() {
	$("#done").show();
	$("#game").hide();

}
function countdown() {
	setInterval(function() {
		update();
		if (time == 0) {
			finishGame();

		}
		time--;

	}, 1000);
}